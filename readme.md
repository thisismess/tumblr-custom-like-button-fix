Tumblr Custom Like Button
=========================
In April 2013, Tumblr updated the code for Like and Reblog buttons in custom themes. It broke a lot of themes that used workarounds [like the one described here](http://like-button.tumblr.com/). This is a somewhat hacky but effective solution.

Markup
------

    <div class="like-container">
        <!--
        Tumblr's like button, transparent and positioned over the
        custom-styled one.
        -->

        {LikeButton}

        <!--
        The custom-styled like button. hover events won't work. You can figure
        out a javascript workaround pretty easily if it's important enough.
        -->
        <a href="#" class="custom-like"></a>
    </div>


CSS
---

Link to the *mess-likebutton.css* file. In your styles, use the adjacent sibling selector to create custom styles for when the post is liked.

    .like_button.liked + .like:before { /* custom liked styles go here */ }